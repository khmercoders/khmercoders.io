<?php

namespace App;

class Helper
{
    public static function getAliasFromString($str)
    {
        // Remove all special characters
        $str = preg_replace('/[^A-Za-z0-9\. ]/', '', $str);

        // Trim all the space
        $str = trim(strtolower($str), ' ');

        // Replace multiple dot with single dot
        $str = preg_replace('/[\. ]+/', '.', $str);

        return $str;
    }
}