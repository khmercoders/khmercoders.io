<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTableDefaultValue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table) {
            // Make name unique because it will be used as an alias
            // in the future for user's profile. 
            // https://khmercodes.io/user/{name}
            $table->string('name')->unique()->change();

            $table->string('position_name')->default('')->change();
            $table->string('job_name')->default('')->change();
            $table->string('picture')->default('')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
