@extends('master')
@section('title', 'Profile Dashboard')


@section('content')

    <div class='container my-5'>
        <h1 class='h4'>Welcome back <span style='color: #584b4f'> {{ Auth::user()->display_name }}  </span></h1>
        <hr>
 
        <div style='margin: 50px 0px; max-width: 500px;'>
            
            @foreach ($users as $user)
                <div class="member-profile-list">
                    <img class="img-circle" src=' {{ $user->getPicture() }} '/>
                </div>         
            @endforeach             
            
            {{ Form::open( array ('action' => ['UsersController@edit', Auth::user()->id], 'method' => 'POST')) }}
                <label>Profile name: </label>
                <p><strong>  {{Auth::user()->display_name}} </strong></p>

                <br>
                <label> Job Position:</label>
                <p><strong> {{Auth::user()->job_name}} </strong></p>
                
                {{ Form::submit('Edit Profile',['class' => 'btn btn-default']) }}
            {{ Form::close() }}
                        
        </div>
        


    </div>

@endsection
